import Sequelize from 'sequelize';

import {database as sequelize} from '../utils/database';
import { Transaction } from './transaction';

export interface IUser extends Sequelize.Model {
    id: number,
    email: number,
    password: string,
    name: string,
    balance: number
}

export const User = sequelize.define<IUser>('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    balance: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});
User.hasMany(Transaction);
