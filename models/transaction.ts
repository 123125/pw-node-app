import Sequelize from 'sequelize';

import {database as sequelize} from '../utils/database';

export interface ITransaction extends Sequelize.Model {
    id: number,
    name: string,
    amount: number,
    balance: number,
    userId: number
}

export const Transaction = sequelize.define<ITransaction>('transaction', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    amount: {
        type: Sequelize.DECIMAL(20, 2),
        allowNull: false
    },
    balance: {
        type: Sequelize.DECIMAL(20, 2),
        allowNull: false
    },
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});
