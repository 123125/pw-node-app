import express from 'express';

import { AuthController } from './auth.controller'
import { registerValidators, loginValidators } from '../../utils/validators';
import validationResult from '../../middleware/validationResult';

const router = express.Router();
const authController = new AuthController();

router.post('/users', registerValidators, validationResult, authController.register);
router.post('/sessions/create', loginValidators, validationResult, authController.login);

export default router;