import bcrypt from 'bcrypt';
import { Request } from 'express';
import jwt from 'jsonwebtoken';

import { User, IUser } from '../../models/user';

export default {
    createToken (email: string, name: string): string {
        return jwt.sign( { email, name }, process.env.JWT_SECRET, { expiresIn: '1h' } );
    },

    async saveSession(req: Request, user: IUser): Promise<void> {
        (req as any).session.user = user;
        (req as any).session.isAuthenticated = true;
        await (req as any).session.save();
    },

    async getUserByEmail(email: string): Promise<IUser> {
        return await User.findOne({where: { email }, raw: true});
    },

    async createUser(req): Promise<IUser> {
        const { email, password, username } = req.body;
        const hashPassword = await bcrypt.hash(password, 10);
        return await User.create({
                email,
                password: hashPassword,
                name: username,
                balance: 500,
            }).then(user => user)
    },

   async findUser(whereOnj): Promise<IUser>  {
        return await User.findOne( { where: whereOnj, raw: true} ).catch(e => {
            console.log(e);
            return null;
        });
    }
 }