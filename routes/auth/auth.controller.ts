import { Request, Response } from 'express';

import authService from './auth.service';
import serverErrorHandler from '../../utils/server-error';

export class AuthController {
    
    public register = async (req: Request, res: Response) => {
        try {
            const user: any = await authService.createUser(req);
            const token = await authService.createToken(req.body.email, user.name);
            await res.status(200).json({ id_token: token });
        } catch (e) {
            serverErrorHandler(e, res);
        }
    };
    
    public login = async (req: Request, res: Response) => {
        try {
            const { email } = req.body;
            const user: any = await authService.getUserByEmail(email);
            const token = authService.createToken(email, user.name);
            await authService.saveSession(req, user);
            await res.status(200).json({ id_token: token });
        } catch (e) {
            serverErrorHandler(e, res);
        }
    };
}
