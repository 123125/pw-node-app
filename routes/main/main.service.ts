import { Request, Response } from 'express';

import { User, IUser } from '../../models/user';
import { ITransaction, Transaction } from '../../models/transaction';

export default {
    async getUsersList (req: Request): Promise<{name: string, id: number}[]> {
        return await User.findAll({
            attributes: ['id', 'name']
        }).then(users => users.filter((item: {name: string, id: number}) => {
                return item.name.includes(req.body.filter) && item.id.toString() !== (req as any).session.user.id.toString();
            })
        );
    },

    async findTransactionsById(id: number): Promise<ITransaction[]> {
        return await Transaction.findAll( { where: { userId: id}, raw: true } );
    },

    async createTransaction(req: Request, updatedBalance: number): Promise<ITransaction> {
        return await Transaction.create({
            name: req.body.name,
            amount: +req.body.amount,
            balance: +updatedBalance,
            userId: (req as any).session.user.id
        }).then(transaction => transaction);
    },

    async findAndUpdateRecipient(req: Request): Promise<void> {
        const recipient: IUser = await User.findOne({ where: {name: req.body.name}, raw: true });
        recipient.balance += +req.body.amount;
        await User.update(recipient, {where: { id: recipient.id} } );
    },

    async updateUserBalance(req: Request, updatedBalance: number): Promise<void> {
        const updatedUser: IUser = {
            ...(req as any).session.user,
            balance: +updatedBalance
        }
        await User.update(updatedUser, {where: { id: updatedUser.id} } );
        (req as any).session.user = updatedUser;
        await (req as any).session.save();
    }
}