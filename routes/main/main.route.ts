import express from 'express';

import { createTransactionValidators } from '../../utils/validators';
import validationResult from '../../middleware/validationResult';
import { MainController } from './main.controller';

const router = express.Router();
const mainController = new MainController();

router.get('/transactions', mainController.getTransactions);
router.post('/transactions', createTransactionValidators, validationResult, mainController.createTransactions);
router.get('/user-info', mainController.getUserInfo);
router.post('/users/list', mainController.getUsersList);

export default router;