import { Request, Response } from 'express';
import mainService from './main.service';

import serverErrorHandler from '../../utils/server-error';
import { ITransaction } from '../../models/transaction';
import { IUser } from '../../models/user';

export class MainController {

    public getTransactions = async (req: Request, res: Response): Promise<void> => {
        try {
            const transactions: ITransaction[] = await mainService.findTransactionsById((req as any).session.user.id);
            res.status(200).json({
                trans_token: transactions
            })
        } catch (e) {
            serverErrorHandler(e, res);
        }
    }

    public createTransactions = async (req: Request, res: Response): Promise<void> => {
        try {
            const updatedBalance: number = (req as any).session.user.balance - req.body.amount;
    
            await mainService.updateUserBalance(req, updatedBalance);
            await mainService.findAndUpdateRecipient(req);
    
            const transaction: ITransaction = await mainService.createTransaction(req, updatedBalance);
            res.status(200).json({ trans_token: transaction });
        } catch (e) {
            serverErrorHandler(e, res);
        }
    };

    public getUserInfo = async (req: Request, res: Response): Promise<Response> => {
        try {
            if (!(req as any).session.user) {
                return res.status(400).json({
                    code: 400,
                    message: 'User not found'
                });
            }
    
            const userInfo: IUser = (req as any).session.user;
            delete userInfo.password;
            return res.status(200).json(userInfo);
        } catch (e) {
            serverErrorHandler(e, res);
        }
    };

    public getUsersList =  async (req: Request, res: Response): Promise<Response> => {
        try {
            if (!req.body.filter) {
                return res.status(401).json({
                    code: 401,
                    message: 'No search string'
                });
            }
    
            const users: {name: string, id: number}[] = await mainService.getUsersList(req);
            return res.status(200).json([ ...users ]);
        } catch (e) {
            serverErrorHandler(e, res);
        }
    };

}
