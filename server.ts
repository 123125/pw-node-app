import * as dotenv from 'dotenv';
dotenv.config();

import express from 'express';
import bodyParser from 'body-parser';
import session from 'express-session';
import SequelizeStore from 'connect-session-sequelize';
import helmet from 'helmet';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';

import { stream } from './utils/logger';
import { database as sequelize } from './utils/database';
import checkJwt from './middleware/checkjwt';
import errorHandler from './middleware/error';

import authRoutes from './routes/auth/auth.route';
import mainRoutes from './routes/main/main.route';

const app = express();
const sequelizeStore = SequelizeStore(session.Store);

// middleware:
app.use(morgan('combined', {"stream": stream }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser())
app.use(helmet());
app.use(cors());

// session:
const store = new sequelizeStore({
    db: sequelize
});
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    store
}));

// routers:
app.use('', authRoutes);
app.use('/api/protected', checkJwt, mainRoutes);

// 404 error:
app.use(errorHandler);

async function start() {
    try {
        await sequelize.sync();
        app.listen(process.env.PORT, function() {
            console.log(`Server is running on port ${process.env.PORT}`);
        });
    } catch (e) {
        console.log(e)
    }
}

start();