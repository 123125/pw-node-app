import winston from './logger';

export default function (error, res): void {
    console.log(error);
    const errorResponse = {
        code: 500, 
        message: 'Server error'
    }
    winston.log('error', 'Error:', error);
    res.status(500).json(errorResponse)
}