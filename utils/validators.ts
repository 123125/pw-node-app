import { body, ValidationChain } from 'express-validator';
import bcrypt from 'bcrypt';

import { IUser } from '../models//user';
import authService from '../routes/auth/auth.service'

export const registerValidators: ValidationChain[] = [
    body('email')
        .isEmail().withMessage('Invalid Email')
        .custom(async (email: string) => {
            const user: IUser = await authService.findUser({email});
            if (user) {
                return Promise.reject('A user with that email already exists')
            }
        })
        .normalizeEmail(),
    body('password')
        .isLength({min: 6, max: 30 }).withMessage('Password\'s length has to be at least 3 symbols')
        .isAlphanumeric()
        .trim(),
    body('username')
        .isLength({ min: 3 }).withMessage('Name\'s length has to be at least 3 symbols')
        .trim()
];

export const loginValidators: ValidationChain[] = [
    body('email')
        .isEmail().withMessage('Invalid Email')
        .custom(async (email: string) => {
            const user: IUser = await authService.findUser({email});
            if (!user) {
                return Promise.reject('A user with that email doesn\'t exist');
            }
        })
        .normalizeEmail(),
    body('password')
        .custom(async (password: string, { req }) => {
            const user: IUser = await authService.findUser({email: req.body.email });
            if (user) {
                const isSame: boolean = await bcrypt.compare(password, user.password).catch(e => console.log(e));
                if (!isSame) {
                    return Promise.reject('Invalid password');
                }
            }
        })
];

export const createTransactionValidators: ValidationChain[] = [
    body('name')
        .custom(async (name: string, { req }) => {
            const user: IUser = await authService.findUser({ name });
            if (!user || user.id.toString() === req.session.user.id.toString()) {
                return Promise.reject('User not found');
            }
        })
        .trim(),
    body('amount')
        .isNumeric().withMessage('Amount has to be number')
        .custom(async (amount: number) => {
            if (amount <= 0) {
                return Promise.reject('Amount has to be more than 0')
            }
        })
        .custom(async (amount: number, { req }) => {
            const updatedBalance: number = req.session.balance - amount;
            if (updatedBalance < 0) {
                return Promise.reject('Balance exceeded')
            }
        })
        .customSanitizer((amount: number) => {
            return Math.abs(amount);
        })
];