import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';

export default (req: Request, res: Response, next: NextFunction) => {

    let token = req.headers['authorization'];
    let jwtPayload;

    try {
        token = token.replace(/^Bearer\s+/, "");
        jwtPayload = jwt.verify(token, process.env.JWT_SECRET);
        res.locals.jwtPayload = jwtPayload;
    } catch (e) {
        console.log(e)
        res.status(401).send({
            code: 401,
            message: 'Unauthorized'
        });
        return;
    }
    next();
}
