import { Request, Response, NextFunction } from 'express';
import { validationResult } from 'express-validator';
import winston from '../utils/logger';

export default async function (req: Request, res: Response, next: NextFunction) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const error = {
            code: 400, 
            message: errors.array()[0].msg
        }
        console.log(error);
        winston.log('error', 'Error:', error);
        return await res.status(400).json(error);
    }
    next();
}